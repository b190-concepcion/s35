const express = require("express");


//this lets us use the mongoose module
const mongoose = require("mongoose");

const app = express();
const port = 3000;


//SECTION - MongoDB connection

/*
	mongoose.connect("<MongoDB atlas connection string>",{
	useNewUrlParser: true,
	useUnifiedTopology: true
} )
*/


//connect to the data by passing in your connection string(from MongoDB)
// .connect() lets us connect and access the database that is sent to it via string.
//b190-to-do is the database that we have created in our MongoDB
mongoose.connect("mongodb+srv://aronsc26:diwa1234@wdc028-course-booking.sysnpgd.mongodb.net/b190-to-do?retryWrites=true&w=majority",
	//this will not prevent Mongoose from being used in application and sending unnecessary warnings when we send requests
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
	);

let db = mongoose.connection;

db.on("error", console.error.bind(console,"connection error"));
db.once("open", () => console.log("we're connected to the database"));

//SECTION - Schema
//Schema() is a method inside the mongoose module that lets us create schema for our database it receives an object with properties and data types that each property shoud have
const taskSchema = new mongoose.Schema({
	//the "name" property should receive a string data type(the data type should be written in Sentence case)
	name: String,
	status:{
		type: String,
		default: "pending"
	}
});

//SECTION - Models

const Task = mongoose.model("Task", taskSchema);

app.use(express.json());
app.use(express.urlencoded({extended: true}));

//Create new task
/*
	BUSINESS LOGIC
		-add functionality that will check if there are duplicate tasks
			-if the task is already existing, we return an error
			-if task is not existing, we add it in the database
		-the task will be sent from the reqeust body
		-create a new Task object with "name" field/property
		-the "status" property does not need to be provided because our schema defaults it to "pending" upon creation of an object
		*/

		app.post("/tasks",(req,res)=>{
			Task.findOne({name: req.body.name}, (err,result) => {
				if(result !== null && result.name === req.body.name){
					return res.send("Duplicate task found");
				}else{
					let newTask = new Task({
						name:req.body.name
					});
					newTask.save((saveErr,saveTask) =>{
						if(saveErr){
							return console.error(saveErr);
						}else{
							return res.status(201).send("New task created");
						}
					})
				}
			})
		})

//Mini-Activity
/*
	Business logic
	-retrieve all documents
	-if there are errors, print the error
	-if there are no erros, send a success status back to the cleint
	*/

	app.get("/tasks",(req,res)=>{
		Task.findOne({}, (err,result) => {
			if(result === null ){
				return res.send("Error");
			}else{
				return res.status(200).json({
					data: result
				});
			};
		});
	});


/*	app.get("/tasks",(req,res)=>{
	Task.find({}, (err,result) => {
		if(result === null){
			return res.send("Error")
		}else{
			res.send(result)
		}
	})
})*/


//SECTION- ACTIVITY
const taskSchema2 = new mongoose.Schema({
	username: String,
	password: String
});

const Task2 = mongoose.model("Task2", taskSchema2);

	app.post("/signup",(req,res)=>{
			Task2.findOne({username: req.body.username, password :req.body.password}, (err,result) => {
				if(result === req.body.username && result === req.body.password){
					return res.send("Error");
				}else{
					let newTask2 = new Task2({
						username:req.body.username,
						password:req.body.password

					});
					newTask2.save((saveErr,savedTask) =>{
						if(saveErr){
							return console.error(saveErr);
						}else{
							return res.status(201).send("New User Created");
						}
					})
				}
			})
		})



	app.listen(port,() => console.log(`Server Running at port ${port}`));